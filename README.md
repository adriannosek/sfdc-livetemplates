# README #

SFDC Live Templates

## What is this repository for?

It's a repository for storing our custom Live Templates for IntelliJ IDEA.
We are a Salesforce Developers that want to make development easier, faster and smarter.

## Live Templates
**Salesforce Lightning**

* **cf** - Creates new controller function
* **hf** - Creates new helper function
* **aa** - Creates Apex action caller

**Salesforce Apex**

* **bset** - Creates a method for SObject builder.
* **tm** - Creates an @IsTest method.
* **ts** - Creates a @TestSetup method.
* **rt** - Snippet: `return this;`.
* **sdv** - Snippet: `System.debug('variable_name: ' + variable_name)`.
* **rb** - Snippet: `Database.rollback(savepoint);`.
* **ss** - Snippet: `Database.Savepoint savepoint = Database.setSavepoint();`.
* **sne** - Snippet: `System.assert(!variable);`.
* **stst** - Inserts `Test.startTest();` and `Test.stopTest();` methods.
* **rn** - Snippet `return null;`.
* **ni** - Snippet, to create a new instance of an object, assigning it to variable: `Object variable = new Object();`.
* **fail** - Snippet: `System.assert(false, 'Assertion message');`.
* **prsf** - Snippet: `private static final`.

## Usage
1. Copy `Lightning.xml` file from our repository
2. Paste file into folder: `C:\Users\USER_NAME\.IdeaICVERSION\config\templates`
3. Restart IntelliJ IDEA

## Authors

* Adrian Nosek
* Norbert Wieruszewski

